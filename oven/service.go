package oven

import (
	"time"

	"codeberg.org/genofire/golang-lib/file"
	"codeberg.org/genofire/golang-lib/worker"
	ovenAPI "codeberg.org/Mediathek/oven-exporter/api"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// Service for oven related commands
type Service struct {
	log             *zap.Logger
	w               *worker.Worker
	Client          ovenAPI.Client    `config:"client"`
	PolicySignedKey string            `config:"policy_signed_key"`
	PolicyExpire    file.TOMLDuration `config:"policy_expire"`
	StreamCheck     file.TOMLDuration `config:"stream_check"`
	DB              *gorm.DB          `config:"-"`
}

// Run start all related workers on oven service
func (s *Service) Run(log *zap.Logger) {
	s.log = log
	if s.w != nil {
		// TODO error handling
		return
	}
	if s.StreamCheck > 0 {
		s.w = worker.NewWorker(time.Duration(s.StreamCheck), s.checkRunning)
		s.w.Start()
	}
}
