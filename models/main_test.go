package models

import (
	"fmt"

	"codeberg.org/genofire/golang-lib/database"
	"codeberg.org/genofire/golang-lib/web/webtest"
)

func DatabaseForTesting() *database.Database {
	dbConfig := database.Database{
		Testdata: true,
		Debug:    false,
		LogLevel: 0,
	}
	dbConfig.Connection.URI = webtest.DBConnection
	SetupMigration(&dbConfig)
	err := dbConfig.ReRun()
	if err != nil {
		fmt.Println(err.Error())
	}
	return &dbConfig
}
