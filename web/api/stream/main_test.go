package stream

import (
	"codeberg.org/genofire/golang-lib/web"
	"codeberg.org/genofire/golang-lib/web/auth"
	"github.com/gin-gonic/gin"
)

func bindTest(r *gin.Engine, ws *web.Service) {
	Bind(r, ws, nil)
	auth.Register(r, ws)
}
