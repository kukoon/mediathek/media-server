package stream

import (
	"net/http"
	"testing"

	"codeberg.org/genofire/golang-lib/web/webtest"
	"github.com/stretchr/testify/assert"

	// "codeberg.org/genofire/golang-lib/web"

	"codeberg.org/Mediathek/media-server/models"
)

func TestAPIList(t *testing.T) {
	assert := assert.New(t)
	s, err := webtest.NewWithDBSetup(bindTest, models.SetupMigration)
	assert.NoError(err)
	defer s.Close()
	assert.NotNil(s)

	list := []*models.PublicStream{}
	// GET
	err = s.Request(http.MethodGet, "/api/v1/streams", nil, http.StatusOK, &list)
	assert.NoError(err)
	assert.Len(list, 22)

	//
	// Test From
	//
	list = []*models.PublicStream{}
	// GET - from all (testdata01)
	err = s.Request(http.MethodGet, "/api/v1/streams?from=2021-03-04T16:21:00.000Z", nil, http.StatusOK, &list)
	assert.NoError(err)
	assert.Len(list, 22, "all from first")

	list = []*models.PublicStream{}
	// GET - from all (testdata01)
	err = s.Request(http.MethodGet, "/api/v1/streams?from=2021-09-03T15:50:00.000Z", nil, http.StatusOK, &list)
	assert.NoError(err)
	assert.Len(list, 1, "from")

	//
	// Test To
	//
	list = []*models.PublicStream{}
	// GET - to all (testdata22)
	err = s.Request(http.MethodGet, "/api/v1/streams?to=2021-09-03T18:59:00.000Z", nil, http.StatusOK, &list)
	assert.NoError(err)
	assert.Len(list, 22, "all to last")

	list = []*models.PublicStream{}
	// GET - to without testdata22
	err = s.Request(http.MethodGet, "/api/v1/streams?to=2021-09-03T15:50:00.000Z", nil, http.StatusOK, &list)
	assert.NoError(err)
	assert.Len(list, 21, "to (without testdata22)")

	//
	// Test From and To together
	//
	list = []*models.PublicStream{}
	// GET - stream_testdata22 started and end in given time
	err = s.Request(http.MethodGet, "/api/v1/streams?from=2021-09-03T15:50:00.000Z&to=2021-09-03T18:59:00.000Z", nil, http.StatusOK, &list)
	assert.NoError(err)
	assert.Len(list, 1, "from and to outside of testdata")

	list = []*models.PublicStream{}
	// GET - stream_testdata22 started in given time but ended later
	err = s.Request(http.MethodGet, "/api/v1/streams?from=2021-09-03T15:50:00.000Z&to=2021-09-03T18:50:00.000Z", nil, http.StatusOK, &list)
	assert.NoError(err)
	assert.Len(list, 1, "from outside and to inside of testdata")

	list = []*models.PublicStream{}
	// GET - stream_testdata22 started before and end in given time
	err = s.Request(http.MethodGet, "/api/v1/streams?from=2021-09-03T15:59:00.000Z&to=2021-09-03T18:59:00.000Z", nil, http.StatusOK, &list)
	assert.NoError(err)
	assert.Len(list, 1, "from inside and to outside of testdata")

	list = []*models.PublicStream{}
	// GET - stream_testdata22 started before and end later -> is current in work
	err = s.Request(http.MethodGet, "/api/v1/streams?from=2021-09-03T15:50:00.000Z&to=2021-09-03T18:50:00.000Z", nil, http.StatusOK, &list)
	assert.NoError(err)
	assert.Len(list, 1, "from and to inside testdata")
}
