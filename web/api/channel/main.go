package channel

import (
	"codeberg.org/genofire/golang-lib/web"
	"codeberg.org/Mediathek/media-server/oven"
	"github.com/gin-gonic/gin"
)

// Bind to webservice
func Bind(r *gin.Engine, ws *web.Service, oven *oven.Service, config *ConfigStream) {
	apiRestreamList(r, ws, oven)
	apiRestreamDelete(r, ws, oven)
	apiRestreamAdd(r, ws, oven)
	apiList(r, ws)
	apiListMy(r, ws)
	apiGet(r, ws, config)
	apiPost(r, ws)
	apiPut(r, ws)
	apiDelete(r, ws)
}
