package channel

import (
	"net/http"
	"testing"

	"codeberg.org/genofire/golang-lib/web/webtest"
	"github.com/stretchr/testify/assert"

	"codeberg.org/Mediathek/media-server/models"
)

func TestAPIList(t *testing.T) {
	assert := assert.New(t)
	s, err := webtest.NewWithDBSetup(apiList, models.SetupMigration)
	assert.NoError(err)
	defer s.Close()
	assert.NotNil(s)

	list := []*models.Channel{}
	// GET
	err = s.Request(http.MethodGet, "/api/v1/channels", nil, http.StatusOK, &list)
	assert.NoError(err)
}
