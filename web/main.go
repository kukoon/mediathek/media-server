package web

import (
	"codeberg.org/genofire/golang-lib/web"
	"codeberg.org/genofire/golang-lib/web/api/status"
	"codeberg.org/genofire/golang-lib/web/auth"
	"codeberg.org/genofire/golang-lib/web/metrics"
	"github.com/gin-gonic/gin"

	"codeberg.org/Mediathek/media-server/models"
	"codeberg.org/Mediathek/media-server/oven"
	"codeberg.org/Mediathek/media-server/web/api/channel"
	"codeberg.org/Mediathek/media-server/web/api/event"
	"codeberg.org/Mediathek/media-server/web/api/recording"
	"codeberg.org/Mediathek/media-server/web/api/speaker"
	"codeberg.org/Mediathek/media-server/web/api/stream"
	"codeberg.org/Mediathek/media-server/web/api/tag"
	"codeberg.org/Mediathek/media-server/web/docs"
	"codeberg.org/Mediathek/media-server/web/podcast"
	wsStream "codeberg.org/Mediathek/media-server/web/ws/stream"
)

// Bind to webservice
func Bind(oven *oven.Service, channelConfig *channel.ConfigStream, streamConfig *models.ConfigStream) web.ModuleRegisterFunc {
	return func(r *gin.Engine, ws *web.Service) {
		docs.Bind(r, ws)

		status.Register(r, ws)
		metrics.Register(r, ws)
		auth.Register(r, ws)

		channel.Bind(r, ws, oven, channelConfig)
		recording.Bind(r, ws)
		stream.Bind(r, ws, streamConfig)

		speaker.Bind(r, ws)
		event.Bind(r, ws)
		tag.Bind(r, ws)

		podcast.Bind(r, ws)
		wsStream.Bind(r, ws)

	}
}
