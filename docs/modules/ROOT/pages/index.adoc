= Administrator

The mediathek has the following components:

* media-ui
* media-server
** postgresql database
** oven media engine

== Install
We start to developing a few Helm Charts xref:mediathek-helm::index.adoc[here]

== Usage


The Media-Server has an REST-API.
Documentation is generated, for https://media.kukoon.de[media.kukoon.de] you could found it under https://media.kukoon.de/api/help/index.html[/api/help/index.html]